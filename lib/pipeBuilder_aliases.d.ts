export declare const hasEpoch: (property: string, before: string, after: string) => any;
export declare const oeiv: (...args: any[]) => any;
export declare const ieov: (...args: any[]) => any;
