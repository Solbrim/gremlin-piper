exports.__esModule = true;
var pipeBuilder_1 = require("./pipeBuilder");
var utils_1 = require("./utils");
exports.hasEpoch = function (property, before, after) {
    /*
      destructure this here
      for hasEpoch is a dependency of pipeBuilder,
      but hasEpoch is only called after pipeBuilder has been instantiated
      pipeBuilder -> create call hasEpoch function -> create pipeBuilder -> call hasEpoch -> pipeBuilder
    */
    var __ = pipeBuilder_1.createPipeBuilderLib();
    var out = __.out, gt = __.gt, lt = __.lt;
    var pipe = __.init();
    if (utils_1.nnnu(after)) {
        pipe.where(out('latest').values(property).is(gt(new Date(after).getTime())));
    }
    if (utils_1.nnnu(before)) {
        pipe.where(out('latest').values(property).is(lt(new Date(before).getTime())));
    }
    return pipe.toString();
};
exports.oeiv = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var __ = pipeBuilder_1.createPipeBuilderLib();
    return __.outE(args).inV().toString();
};
exports.ieov = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var __ = pipeBuilder_1.createPipeBuilderLib();
    return __.inE(args).outV().toString();
};
//# sourceMappingURL=pipeBuilder_aliases.js.map