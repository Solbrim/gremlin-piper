import { CreatePipeBuilderOptions, PipeLibary } from './Types';
import { GremlinFunctions } from './GremlinTypes';
export declare const createPipeBuilderLib: <InputType = GremlinFunctions>(options?: CreatePipeBuilderOptions) => PipeLibary<InputType>;
