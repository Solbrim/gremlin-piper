exports.__esModule = true;
var pipeBuilder_1 = require("./pipeBuilder");
var __ = pipeBuilder_1.createPipeBuilderLib();
var g = __.g, inE = __.inE, out = __.out, outE = __.outE, label = __.label, eq = __.eq, incr = __.incr, id = __.id;
// 1
var pipe = g.V().outE();
console.log(pipe.toString());
// console: g.V().outE()
var pipe2 = inE('latest').inV();
pipe.union(pipe2, out('latest'));
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest'));
var pipe3 = __;
pipe3 = pipe3.inE('test').outV().values('test').is(1);
pipe.where(pipe3);
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest')).where(inE('test').outV().values('test').is(1))
var appendTest = outE('append').inV();
pipe.append(appendTest);
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest')).where(inE('test').outV().values('test').is(1)).outE('append').inV()
var cloneTest = outE('test').inV();
var cloned = cloneTest.clone();
cloneTest.where(cloned.append(label().is(eq('test'))));
cloneTest.log();
// console: outE('test').inV().where(outE('test').inV().label().is(eq('test'))));
var incrTest = outE().order().by('test', incr);
incrTest.log();
var chooseIdTest = outE('test').outE('test').inV().choose(id()).option(1, outE('latest').inV()).option(2, outE('latest').inV());
chooseIdTest.log();
var nestedArrayInput = outE(['test', ['test1', 'test2']]).inV();
nestedArrayInput.log();
//# sourceMappingURL=test.js.map