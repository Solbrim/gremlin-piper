import { AliasPipe, PrimitivePipeArgument, LangReservedWordHandler } from './Types';
export declare const p: (input: any, idType?: boolean) => any;
export declare const default_aliasPipes: {
    [pipeName: string]: AliasPipe;
};
export declare const default_aliasPipeNames: string[];
export declare const default_nonFunctionPipeNames: string[];
export declare const default_langReservedWordPipeNames: string[];
export declare const default_regularPipeNames: string[];
export declare const default_langReservedWordHandler: LangReservedWordHandler;
export declare const noPrimitiveArgumentTransformForType: (pipeName: any, arg: any) => Error;
export declare const idPipeNames: string[];
export declare const default_primitiveArgumentTransform: (pipeName: string, arg: PrimitivePipeArgument, forceId?: boolean) => string;
