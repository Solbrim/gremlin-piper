export declare type AliasPipe = (...args: any[]) => string;
export declare type PipeBuilderType = 'pipeBuilder';
export declare type PipeArgument = PipeBuilder & PrimitivePipeArgument;
export declare type PrimitivePipeArgument = number | string | boolean;
export declare type PrimitiveArgumentTransform = (pipeName: string, arg: PrimitivePipeArgument, forceId?: boolean) => string;
export interface PipeBuilder {
    call_pipeNames: string[];
    call_arguments: PipeArgument[][];
    toString: () => string;
    log: () => void;
    clone: () => PipeBuilder;
    append: (secondaryPipeBuilder: PipeBuilder) => PipeBuilder;
    type: PipeBuilderType;
}
export declare type PipeLibary<T> = T & {
    init: () => T;
};
export declare type LangReservedWordHandler = (pipeName: string) => string;
export declare type CreatePipeBuilderOptions = {
    aliasPipeNames?: string[];
    aliasPipes?: {
        [pipeName: string]: AliasPipe;
    };
    langReservedWordHandler?: (pipeName: string) => string;
    langReservedWordPipeNames?: string[];
    nonFunctionPipeNames?: string[];
    primitiveArgumentTransform?: PrimitiveArgumentTransform;
    regularPipeNames?: string[];
};
