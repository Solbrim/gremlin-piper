exports.__esModule = true;
var pipeBuilder_defaults_1 = require("./pipeBuilder_defaults");
exports.createPipeBuilderLib = function (options) {
    if (options === void 0) { options = {}; }
    var aliasPipeNames = options.aliasPipeNames || pipeBuilder_defaults_1.default_aliasPipeNames;
    var regularPipeNames = options.regularPipeNames || pipeBuilder_defaults_1.default_regularPipeNames;
    var langReservedWordHandler = options.langReservedWordHandler || pipeBuilder_defaults_1.default_langReservedWordHandler;
    var langReservedWordPipeNames = options.langReservedWordPipeNames || pipeBuilder_defaults_1.default_langReservedWordPipeNames;
    var nonFunctionPipeNames = options.nonFunctionPipeNames || pipeBuilder_defaults_1.default_nonFunctionPipeNames;
    var primitiveArgumentTransform = options.primitiveArgumentTransform || pipeBuilder_defaults_1.default_primitiveArgumentTransform;
    var aliasPipes = options.aliasPipes || pipeBuilder_defaults_1.default_aliasPipes;
    var toString = function (pipeBuilder) { return function () {
        var script = '';
        var stringifyArgs = function (pipeName, args, idType) {
            if (idType === void 0) { idType = false; }
            return args.map(function (arg) {
                return arg.type &&
                    arg.type === 'pipeBuilder' ?
                    arg.toString() :
                    primitiveArgumentTransform(pipeName, arg, idType);
            }).toString();
        };
        pipeBuilder.call_pipeNames.forEach(function (pipeName, index, a) {
            var args = [];
            var interpretArg = function (a) { return Array.isArray(a) ? a.forEach(function (b) { return interpretArg(b); }) : args.push(a); };
            pipeBuilder.call_arguments[index].forEach(function (arg) { return interpretArg(arg); });
            if (index > 0) {
                script += '.';
            }
            else if (langReservedWordPipeNames.includes(pipeName)) {
                script += langReservedWordHandler(pipeName);
            }
            if (nonFunctionPipeNames.includes(pipeName)) {
                script += pipeName;
            }
            else if (aliasPipeNames.includes(pipeName)) {
                script += aliasPipes[pipeName](args);
            }
            else {
                if (pipeName === 'option') {
                    var chooseIndex = (a.length - 1) - (pipeBuilder.call_pipeNames
                        .map(function (x) { return x; })
                        .reverse()
                        .slice(a.length - (index + 1))
                        .findIndex(function (e) { return e === 'choose'; }) + (a.length - (index + 1)));
                    var chooseId = pipeBuilder.call_arguments[chooseIndex][0].call_pipeNames[0] === 'id';
                    script += pipeName + "(" + stringifyArgs(pipeName, args, chooseId) + ")";
                }
                else if (pipeName === 'wildCode') {
                    script += args[0];
                }
                else {
                    script += pipeName + "(" + stringifyArgs(pipeName, args) + ")";
                }
            }
        });
        return script;
    }; };
    var log = function (pipeBulder) { return function () { return console.log(pipeBulder.toString()); }; };
    var append = function (pipeBuilder) { return function (secondaryPipeBuilder) {
        pipeBuilder.call_arguments = pipeBuilder.call_arguments.concat(secondaryPipeBuilder.call_arguments);
        pipeBuilder.call_pipeNames = pipeBuilder.call_pipeNames.concat(secondaryPipeBuilder.call_pipeNames);
        return pipeBuilder;
    }; };
    var clone = function (pipeBuilder) { return function () {
        var clonedPipeBuilder = {
            call_arguments: [],
            call_pipeNames: pipeBuilder.call_pipeNames.map(function (p) { return p; }),
            type: 'pipeBuilder',
            log: null,
            append: null,
            toString: null,
            clone: null
        };
        pipeBuilder.call_arguments.forEach(function (args) {
            var oneDArray = [];
            args.forEach(function (arg) {
                if (Array.isArray(arg)) {
                    arg.forEach(function (a) {
                        if (a.type && a.type === 'pipeBuilder') {
                            oneDArray.push(a.clone());
                        }
                        else {
                            oneDArray.push(a);
                        }
                    });
                }
                else {
                    oneDArray.push(arg.type && arg.type === 'pipeBuilder' ? arg.clone() : arg);
                }
            });
            clonedPipeBuilder.call_arguments.push(oneDArray);
        });
        return initPipeBuilder(clonedPipeBuilder);
    }; };
    var initPipeBuilder = function (pipeBuilderArg) {
        var pipeBuilder = pipeBuilderArg || {
            call_arguments: [],
            call_pipeNames: [],
            type: 'pipeBuilder',
            log: null,
            toString: null,
            clone: null,
            append: null
        };
        regularPipeNames
            .concat(aliasPipeNames)
            .concat(langReservedWordPipeNames)
            .forEach(function (pipeName) { return Object.defineProperty(pipeBuilder, pipeName, {
            configurable: true,
            get: function () { return function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                pipeBuilder.call_pipeNames.push(pipeName);
                pipeBuilder.call_arguments.push(args || []);
                return pipeBuilder;
            }; }
        }); });
        nonFunctionPipeNames
            .forEach(function (pipeName) { return Object.defineProperty(pipeBuilder, pipeName, {
            configurable: true,
            get: function () {
                pipeBuilder.call_pipeNames.push(pipeName);
                pipeBuilder.call_arguments.push([]);
                return pipeBuilder;
            }
        }); });
        pipeBuilder.toString = toString(pipeBuilder);
        pipeBuilder.log = log(pipeBuilder);
        pipeBuilder.clone = clone(pipeBuilder);
        pipeBuilder.append = append(pipeBuilder);
        return pipeBuilder;
    };
    var pipeLibrary = {
        init: function () { return initPipeBuilder(); }
    };
    regularPipeNames
        .concat(aliasPipeNames)
        .concat(langReservedWordPipeNames)
        .forEach(function (pipeName) { return Object.defineProperty(pipeLibrary, pipeName, {
        configurable: true,
        get: function () { return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = initPipeBuilder())[pipeName].apply(_a, args);
        }; }
    }); });
    nonFunctionPipeNames
        .forEach(function (pipeName) { return Object.defineProperty(pipeLibrary, pipeName, {
        configurable: true,
        get: function () { return initPipeBuilder()[pipeName]; }
    }); });
    return pipeLibrary;
};
//# sourceMappingURL=pipeBuilder.js.map