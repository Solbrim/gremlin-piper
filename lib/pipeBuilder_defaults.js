exports.__esModule = true;
var pipeBuilder_aliases_1 = require("./pipeBuilder_aliases");
exports.p = function (input, idType) {
    return input;
};
exports.default_aliasPipes = {
    ieOv: function (input) { return "inE(" + exports.p(Object.keys(input).map(function (k) { return input[k]; })) + ").outV()"; },
    oeIv: function (input) { return "outE(" + exports.p(Object.keys(input).map(function (k) { return input[k]; })) + ").inV()"; },
    hasEpoch: function (args) { return pipeBuilder_aliases_1.hasEpoch(args[0], args[1], args[2]); }
};
exports.default_aliasPipeNames = Object.keys(exports.default_aliasPipes);
exports.default_nonFunctionPipeNames = [
    'g',
    'none',
    'incr',
    'decr',
];
exports.default_langReservedWordPipeNames = [
    'in',
    'as',
];
exports.default_regularPipeNames = [
    'E',
    'V',
    'math',
    'key',
    'properties',
    'toList',
    'hasNext',
    'next',
    'tryNext',
    'toSet',
    'toBulkSet',
    'fill',
    'iterate',
    'wildCode',
    'barrier',
    'addE',
    'addV',
    'aggregate',
    'and',
    'as',
    'bothE',
    'by',
    'cap',
    'choose',
    'coalesce',
    'coin',
    'constant',
    'count',
    'cyclicPath',
    'dedup',
    'drop',
    'emit',
    'eq',
    'fold',
    'from',
    'group',
    'gt',
    'gte',
    'has',
    'hasId',
    'hasLabel',
    'hasNot',
    'hasValue',
    'identity',
    'id',
    'inE',
    'inV',
    'is',
    'label',
    'limit',
    'lt',
    'lte',
    'max',
    'mean',
    'min',
    'neq',
    'not',
    'option',
    'optional',
    'or',
    'order',
    'otherV',
    'out',
    'outE',
    'outV',
    'path',
    'property',
    'range',
    'repeat',
    'select',
    'sideEffect',
    'sum',
    'to',
    'tree',
    'unfold',
    'union',
    'until',
    'values',
    'where',
    'within',
    'loops',
    'local',
    'store',
    'project',
];
exports.default_langReservedWordHandler = function (pipeName) { return '__.'; };
exports.noPrimitiveArgumentTransformForType = function (pipeName, arg) {
    return new Error("noPrimitiveArgumentTransformForType: pipeName: " + pipeName + ", arg " + arg + ", typeof " + (Array.isArray(arg) ? typeof arg[0] + "[]" : typeof arg));
};
exports.idPipeNames = [
    'V',
    'E',
    'hasId',
];
exports.default_primitiveArgumentTransform = function (pipeName, arg, forceId) {
    var idType = exports.idPipeNames.includes(pipeName);
    switch (typeof arg) {
        case 'string':
            return "'" + arg + "'";
        case 'number':
            return idType || forceId ? arg + "L" : arg.toString();
        case 'boolean':
            return arg.toString();
        default:
            throw exports.noPrimitiveArgumentTransformForType(pipeName, arg);
    }
};
//# sourceMappingURL=pipeBuilder_defaults.js.map