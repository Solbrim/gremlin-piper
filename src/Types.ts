export type AliasPipe = (...args: any[]) => string;
export type PipeBuilderType = 'pipeBuilder';
export type PipeArgument = PipeBuilder & PrimitivePipeArgument;
export type PrimitivePipeArgument = number | string | boolean;
export type PrimitiveArgumentTransform = (pipeName: string, arg: PrimitivePipeArgument, forceId?: boolean) => string;
export interface PipeBuilder  {
  call_pipeNames: string[];
  call_arguments: PipeArgument[][];
  toString: () => string;
  log: () => void;
  clone: () => PipeBuilder;
  append: (secondaryPipeBuilder: PipeBuilder) => PipeBuilder;
  type: PipeBuilderType;
}

export type PipeLibary<T> = T & {
  init: () => T,
}
export type LangReservedWordHandler = (pipeName: string) => string;

export type CreatePipeBuilderOptions = {
  aliasPipeNames?: string[];
  aliasPipes?: {[pipeName: string]: AliasPipe};
  langReservedWordHandler?: (pipeName: string) => string;
  langReservedWordPipeNames?: string[];
  nonFunctionPipeNames?: string[];
  primitiveArgumentTransform?: PrimitiveArgumentTransform;
  regularPipeNames?: string[];
}

