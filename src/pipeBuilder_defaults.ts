import {
  AliasPipe,
  PrimitivePipeArgument,
  LangReservedWordHandler,
} from './Types';
import { hasEpoch } from './pipeBuilder_aliases';
export const p = (input, idType?: boolean) => {
  return input;
}

export const default_aliasPipes: {[pipeName: string]: AliasPipe} = {
  ieOv: input => `inE(${p(Object.keys(input).map(k => input[k]))}).outV()`,
  oeIv: input => `outE(${p(Object.keys(input).map(k => input[k]))}).inV()`,
  hasEpoch: args => hasEpoch(args[0], args[1], args[2]),
};

export const default_aliasPipeNames = Object.keys(default_aliasPipes);

export const default_nonFunctionPipeNames = [
  'g',
  'none',
  'incr',
  'decr',
];
export const default_langReservedWordPipeNames = [
  'in',
  'as',
];
export const default_regularPipeNames = [
  'E',
  'V',
  'math',
  'key',
  'properties',
  'toList',
  'hasNext',
  'next',
  'tryNext',
  'toSet',
  'toBulkSet',
  'fill',
  'iterate',
  'wildCode',
  'barrier',
  'addE',
  'addV',
  'aggregate',
  'and',
  'as',
  'bothE',
  'by',
  'cap',
  'choose',
  'coalesce',
  'coin',
  'constant',
  'count',
  'cyclicPath',
  'dedup',
  'drop',
  'emit',
  'eq',
  'fold',
  'from',
  'group',
  'gt',
  'gte',
  'has',
  'hasId',
  'hasLabel',
  'hasNot',
  'hasValue',
  'identity',
  'id',
  'inE',
  'inV',
  'is',
  'label',
  'limit',
  'lt',
  'lte',
  'max',
  'mean',
  'min',
  'neq',
  'not',
  'option',
  'optional',
  'or',
  'order',
  'otherV',
  'out',
  'outE',
  'outV',
  'path',
  'property',
  'range',
  'repeat',
  'select',
  'sideEffect',
  'sum',
  'to',
  'tree',
  'unfold',
  'union',
  'until',
  'values',
  'where',
  'within',
  'loops',
  'local',
  'store',
  'project',
];

export const default_langReservedWordHandler: LangReservedWordHandler = (pipeName: string) => '__.';

export const noPrimitiveArgumentTransformForType = (pipeName, arg) =>
  new Error(`noPrimitiveArgumentTransformForType: pipeName: ${pipeName}, arg ${arg}, typeof ${Array.isArray(arg) ? `${typeof arg[0]}[]` : typeof arg}`);

export const idPipeNames = [
  'V',
  'E',
  'hasId',
];
export const default_primitiveArgumentTransform = (pipeName: string, arg: PrimitivePipeArgument, forceId?: boolean): string => {
  const idType = idPipeNames.includes(pipeName);
  switch (typeof arg) {
    case 'string':
      return `'${arg}'`;
    case 'number':
      return idType || forceId ? `${arg}L` : arg.toString();
    case 'boolean':
      return arg.toString();
    default:
      throw noPrimitiveArgumentTransformForType(pipeName, arg);
  }
};


