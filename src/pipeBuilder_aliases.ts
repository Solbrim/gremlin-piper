
import { createPipeBuilderLib } from './pipeBuilder';
import { nnnu } from './utils';
export const hasEpoch = (property: string, before: string, after: string) => {
  /*
    destructure this here
    for hasEpoch is a dependency of pipeBuilder,
    but hasEpoch is only called after pipeBuilder has been instantiated
    pipeBuilder -> create call hasEpoch function -> create pipeBuilder -> call hasEpoch -> pipeBuilder
  */
  const __: any = createPipeBuilderLib();
  const { out, gt, lt } = __;
  let pipe = __.init();
  if (nnnu(after)) {
    pipe.where(out('latest').values(property).is(gt(new Date(after).getTime())));
  }
  if (nnnu(before)) {
    pipe.where(out('latest').values(property).is(lt(new Date(before).getTime())));
  }
  return pipe.toString();
};

export const oeiv = (...args) => {
  const __: any = createPipeBuilderLib();
  return __.outE(args).inV().toString();
}

export const ieov = (...args) => {
  const __: any = createPipeBuilderLib();
  return __.inE(args).outV().toString();
}
