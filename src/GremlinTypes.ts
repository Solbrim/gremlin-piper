import {
  PipeBuilder,
} from './Types';

export type incr = 'incr';
export type decr = 'decr';
export type ByFunction = (arg0: string | PipeBuilder, arg1?: incr | decr) => PipeBuilder;
export type VoidFunction = () => PipeBuilder;
export type BooleanSpreadFunction = (...args: boolean[]) => PipeBuilder;
export type DuoNumberFunction = (arg0: number, arg1: number) => PipeBuilder;
export type NumberFunction = (arg: number) => PipeBuilder;
export type NumberSpreadFunction = (...args: number[]) => PipeBuilder;
export type NumberBuilderFunction = (arg: number | PipeBuilder) => PipeBuilder;
export type IdSpreadFunction = (...args: (string | number)[]) => PipeBuilder;
export type StringSpreadFunction = (...args: string[]) => PipeBuilder;
export type StringNumberFunction = (arg: String | Number) => PipeBuilder;
export type StringBuilderFunction = (arg: string | PipeBuilder) => PipeBuilder;
export type UniStringFunction = (arg: string) => PipeBuilder;
export type BuilderSpreadFunction = (...args: PipeBuilder[]) => PipeBuilder;
export type BuilderFunction = (arg: PipeBuilder) => PipeBuilder;
export type DuoBuilderFunction = (arg0: PipeBuilder, arg1: PipeBuilder) => PipeBuilder;
export type PropertyFunction = (arg0: string, arg1: string | number | boolean) => PipeBuilder;
export type HasFunction = (arg0: string, arg1: string | number | boolean | PipeBuilder, arg2?: string) => PipeBuilder;
export type OptionFunction = (arg0: string, arg1: PipeBuilder) => PipeBuilder;
export type PrimitiveFunction = (arg: string | number | boolean) => PipeBuilder;
export type PrimitiveSpreadFunction = (...args: (string | number | boolean)[]) => PipeBuilder;

export interface GremlinFunctions {
  g?: PipeBuilder;
  V?: IdSpreadFunction;
  E?: IdSpreadFunction;
  barrier?: VoidFunction;
  addE?: StringSpreadFunction;
  addV?: StringSpreadFunction;
  aggregate?: StringSpreadFunction;
  and?: DuoBuilderFunction | VoidFunction;
  as?: UniStringFunction;
  by?: ByFunction;
  choose?: BuilderFunction;
  coalesce?: DuoBuilderFunction;
  count?: VoidFunction;
  drop?: VoidFunction;
  emit?: BuilderFunction;
  eq?: StringNumberFunction;
  from?: StringBuilderFunction;

  gt?: StringNumberFunction;
  gte?: StringNumberFunction;

  has?: HasFunction;
  hasLabel?: StringSpreadFunction;
  hasNot?: StringSpreadFunction;
  hasValue?: PrimitiveSpreadFunction;
  hasId?: IdSpreadFunction;
  hasKey?: StringSpreadFunction;

  identity?: VoidFunction;
  inE?: StringSpreadFunction;
  inV?: StringSpreadFunction;
  is?: NumberBuilderFunction;
  label?: VoidFunction;
  local?: BuilderFunction;
  limit?: NumberFunction;
  tail?: NumberFunction;
  lt?: StringNumberFunction;
  lte?: StringNumberFunction;
  neq?: StringNumberFunction;
  not?: BuilderFunction;
  min?: VoidFunction;
  max?: VoidFunction;
  mean?: VoidFunction;
  option?: OptionFunction;
  optional?: BuilderFunction;
  or?: DuoBuilderFunction | VoidFunction;
  order?: VoidFunction;
  out?: StringSpreadFunction;
  outE?: StringSpreadFunction;
  outV?: StringSpreadFunction;
  path?: VoidFunction;
  property?: PropertyFunction;
  range?: DuoNumberFunction;
  repeat?: BuilderFunction;
  select?: StringSpreadFunction;
  sum?: VoidFunction;
  fold?: BuilderFunction;
  unfold?: VoidFunction;
  to?: StringSpreadFunction;
  tree?: VoidFunction;
  union?: DuoBuilderFunction;
  until?: BuilderFunction;
  values?: StringSpreadFunction;
  where?: BuilderFunction;
  within?: PrimitiveSpreadFunction;
}
