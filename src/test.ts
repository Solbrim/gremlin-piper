import { createPipeBuilderLib } from './pipeBuilder';
const __ = createPipeBuilderLib() as any;
const { g, inE, out, outE, label, eq, incr, id } = __;

// 1
const pipe = g.V().outE();
console.log(pipe.toString());
// console: g.V().outE()

const pipe2 = inE('latest').inV();
pipe.union(pipe2, out('latest'));
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest'));

let pipe3 = __;
pipe3 = pipe3.inE('test').outV().values('test').is(1);
pipe.where(pipe3);
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest')).where(inE('test').outV().values('test').is(1))

let appendTest = outE('append').inV();
pipe.append(appendTest);
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest')).where(inE('test').outV().values('test').is(1)).outE('append').inV()

let cloneTest = outE('test').inV();
const cloned = cloneTest.clone();
cloneTest.where(cloned.append(label().is(eq('test'))));
cloneTest.log()
// console: outE('test').inV().where(outE('test').inV().label().is(eq('test'))));

let incrTest = outE().order().by('test', incr);
incrTest.log();

let chooseIdTest = outE('test').outE('test').inV().choose(id()).option(1, outE('latest').inV()).option(2, outE('latest').inV());
chooseIdTest.log();

let nestedArrayInput = outE(['test', ['test1', 'test2']]).inV()
nestedArrayInput.log();
