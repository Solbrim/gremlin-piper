import {
  CreatePipeBuilderOptions,
  PipeBuilder,
  PipeArgument,
  PipeLibary,
} from './Types';
import { GremlinFunctions } from './GremlinTypes';

import {
  default_aliasPipeNames,
  default_aliasPipes,
  default_langReservedWordHandler,
  default_langReservedWordPipeNames,
  default_primitiveArgumentTransform,
  default_regularPipeNames,
  default_nonFunctionPipeNames,
} from './pipeBuilder_defaults'



export const createPipeBuilderLib = <InputType = GremlinFunctions>(options: CreatePipeBuilderOptions = {}) => {
    type T = InputType & PipeBuilder;

    const aliasPipeNames = options.aliasPipeNames || default_aliasPipeNames;
    const regularPipeNames = options.regularPipeNames || default_regularPipeNames;
    const langReservedWordHandler = options.langReservedWordHandler || default_langReservedWordHandler;
    const langReservedWordPipeNames =  options.langReservedWordPipeNames || default_langReservedWordPipeNames;
    const nonFunctionPipeNames = options.nonFunctionPipeNames || default_nonFunctionPipeNames;
    const primitiveArgumentTransform = options.primitiveArgumentTransform || default_primitiveArgumentTransform;
    const aliasPipes = options.aliasPipes || default_aliasPipes;

    const toString = (pipeBuilder: T) => () => {
      let script = '';

      const stringifyArgs = (pipeName: string, args: PipeArgument[], idType = false) =>
        args.map(arg =>
          arg.type &&
          arg.type === 'pipeBuilder' ?
          arg.toString() :
          primitiveArgumentTransform(pipeName, arg, idType)
        ).toString()

      pipeBuilder.call_pipeNames.forEach((pipeName, index, a) => {
        const args = [];
        const interpretArg = a => Array.isArray(a) ? a.forEach(b => interpretArg(b)) : args.push(a);
        pipeBuilder.call_arguments[index].forEach(arg => interpretArg(arg));
        if (index > 0) {
          script += '.';
        } else if (langReservedWordPipeNames.includes(pipeName)) {
          script += langReservedWordHandler(pipeName);
        }
        if (nonFunctionPipeNames.includes(pipeName)) {
          script += pipeName;
        } else if (aliasPipeNames.includes(pipeName)) {
          script += aliasPipes[pipeName](args);
        } else {
          if (pipeName === 'option') {
            const chooseIndex = (a.length - 1) - (pipeBuilder.call_pipeNames
              .map(x => x)
              .reverse()
              .slice(a.length - (index + 1))
              .findIndex(e => e === 'choose') + (a.length - (index + 1)));

            const chooseId = pipeBuilder.call_arguments[chooseIndex][0].call_pipeNames[0] === 'id';
            script += `${pipeName}(${stringifyArgs(pipeName, args, chooseId)})`
          } else if (pipeName === 'wildCode') {
            script += args[0];
          } else {
            script += `${pipeName}(${stringifyArgs(pipeName, args)})`;
          }
        }
      });
      return script;
    };
    const log = (pipeBulder: T) => () => console.log(pipeBulder.toString());
    const append = (pipeBuilder: T) => (secondaryPipeBuilder: T) => {
      pipeBuilder.call_arguments = pipeBuilder.call_arguments.concat(secondaryPipeBuilder.call_arguments);
      pipeBuilder.call_pipeNames = pipeBuilder.call_pipeNames.concat(secondaryPipeBuilder.call_pipeNames);
      return pipeBuilder;
    }
    const clone = (pipeBuilder: T) => () => {
      const clonedPipeBuilder: T = {
        call_arguments: [],
        call_pipeNames: pipeBuilder.call_pipeNames.map(p => p),
        type: 'pipeBuilder',
        log: null,
        append: null,
        toString: null,
        clone: null,
      } as T;

      pipeBuilder.call_arguments.forEach((args) => {
        const oneDArray = [];
        args.forEach((arg) => {
          if (Array.isArray(arg)) {
            arg.forEach(a => {
              if (a.type && a.type === 'pipeBuilder') {
                oneDArray.push(a.clone());
              } else {
                oneDArray.push(a);
              }
            });
          } else {
            oneDArray.push(arg.type && arg.type === 'pipeBuilder' ? arg.clone() : arg);
          }
        });
        clonedPipeBuilder.call_arguments.push(oneDArray);
      });

      return initPipeBuilder(clonedPipeBuilder);
    }


    const initPipeBuilder = (pipeBuilderArg?: T) => {
      const pipeBuilder: T = pipeBuilderArg || {
        call_arguments: [],
        call_pipeNames: [],
        type: 'pipeBuilder',
        log: null,
        toString: null,
        clone: null,
        append: null,
      } as T;
      regularPipeNames
        .concat(aliasPipeNames)
        .concat(langReservedWordPipeNames)
        .forEach(pipeName => Object.defineProperty(pipeBuilder, pipeName, {
          configurable: true,
          get: () => (...args) => {
            pipeBuilder.call_pipeNames.push(pipeName);
            pipeBuilder.call_arguments.push(args || []);
            return pipeBuilder;
          },
        }));
      nonFunctionPipeNames
       .forEach(pipeName => Object.defineProperty(pipeBuilder, pipeName, {
        configurable: true,
        get: () => {
          pipeBuilder.call_pipeNames.push(pipeName);
          pipeBuilder.call_arguments.push([]);
          return pipeBuilder;
        }
       }));

      pipeBuilder.toString = toString(pipeBuilder);
      pipeBuilder.log = log(pipeBuilder);
      pipeBuilder.clone = clone(pipeBuilder);
      pipeBuilder.append = append(pipeBuilder);

      return pipeBuilder;
    }

    const pipeLibrary: PipeLibary<InputType> = {
      init: () => initPipeBuilder(),
    } as PipeLibary<T>;

    regularPipeNames
      .concat(aliasPipeNames)
      .concat(langReservedWordPipeNames)
      .forEach(pipeName => Object.defineProperty(pipeLibrary, pipeName, {
        configurable: true,
        get: () => (...args) => initPipeBuilder()[pipeName](...args),
      }));

    nonFunctionPipeNames
      .forEach(pipeName => Object.defineProperty(pipeLibrary, pipeName, {
        configurable: true,
        get: () => initPipeBuilder()[pipeName],
      }));

    return pipeLibrary;
};
