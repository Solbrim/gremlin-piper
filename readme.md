# gremlin-piper

## TODO:
  0. fix exports
  1. properly integrate typescript types
  2. implement the gremlin pipes missed in the initial pass
  3. Add a clone generator function that returns a clone of the specific type everytime it is called
  4. Make sure the repo is consistant with what is explained in the readme.md
  7. Add character escaping to default_primitiveValueTransform
  8. Add a default_aliasPipe, which can take multiple pipes, and chooses which to print when toString is called based on input args
  11. Figure out why some objects need to be cast with as in order to shut typescript up
  12. Add init function, which returns a pipeBuilder, but doesnt add anything to the callstack

## usage example:

```javascript
import { createPipeBuilderLib } from 'pipeBuilder';
const { g, inE } = createPipeBuilderLib();

// 1
const pipe = g.V().outE();
console.log(pipe.toString());
// console: g.V().outE()

const pipe2 = inE('latest').inV();
pipe.union(pipe2, out('latest'));
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest'));

let pipe3 = __;
pipe3 = pipe3.inE('test').outV().values('test').is(1);
pipe.where(pipe);
console.log(pipe3.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest')).where(inE('test').outV().values('test').is(1))

let appendTest = outE('append').inV();
pipe.append(appendTest);
console.log(pipe.toString());
// console: g.V().outE().union(inE('latest').inV(),out('latest')).where(inE('test').outV().values('test').is(1)).outE('append').inV()

let cloneTest = outE('test').inV();
const cloned = cloneTest.clone();
cloneTest.where(cloned.append(label().is(eq('test'))));
cloneTest.log()
// console: outE('test').inV().where(outE('test').inV().label().is(eq('test'))));
```

## glossary:

#### Builder
  The main object returned by createdBuilder and any given pipe.

#### pipe
  A term used to describe any function or property that mutates the Builder internal state
    and returns the Builder the pipe is a member of.

#### pipeNames
  pipeNames are a generic term used for any property that will be accessable
    at any point in the created pipeBuilder.

## override defaults:
  out-of-the-box, pipeBuilder is set up with gremlin traversal scripts in mind, but createPipeBuilder
    takes several arguments to override or extend this functionality.

### aliasPipeNames?: string[]
  Pipes with custom functionality

### aliasPipes?: {[pipeName: string]: AliasPipe};
  A map of functions, which return a string, and will be called once toString
    is called.

### langReservedWordPipeNames?: string[];
  Pipes that are also reserved words in the output programming language.
    When these pipes are stringified, if they are at the head of the pipeStream,
    they will be prepended with __.

### nonFunctionPipeNames?: string[];
  Pipes that are not functions, but merely properties.
    toString will not look for arguments for these pipes, and will not
    append them with paraentheses.

### primitiveArgumentTransform?: PrimitiveArgumentTransform;
  A function that is used to stringify the pipe arguments.
    PipeName: string and the argument be processed: any
    are passed into this function when its called.

### regularPipeNames?: string[];
  A list of generic pipes.

### langReservedWordHandler?: string;
  A function that takes a pipeName as its input, and returns a string
    which will be appended to the output script, if the given
    reserved word is a head of a pipeStream
  default: appended __. to the script;

## gotchas:

#### Infinite toString recursion when passing a destructured nonFunctionProp in as an argument of its own pipeStream.

  ```javascript
  import { createPipeBuilderLib } from 'pipe-builder-to-string';
  const __ = createPipeBuilderLib();
  const { g } = __;
  const pipe = g.V().where(g.V().inE().label().is(eq('one')));
  ```

  ...will result in a stack overflow upon calling toString.

  When properties are deconstructed from createPipeBuilder, its get
    function constructs a new pipeBuilder.
  So, if one were to construct a pipe with g, then pass g into a g pipe call,
    upon running toString, the toString function will attempt to stringify
    the nested g, and since they're the same builder, will also contain said g

  ...you get the picture.

  A potential work around might look like...
  ```javascript
  const pipe = g.V().where(__.g.V().inE().label().is(eq('one')));
  ```

  This way, g.get() is called a second time, and a new pipeBuilder is created

  If there are any suggestions, which maintain the syntax, I'm all open.
  Personally, I don't really see this as a huge deal breaker, since, with keeping Gremlin in mind,
    calling g isn't something you're going to do more than once.

#### primitiveValueTransform throwing an error, saying it cannot handle [object Object] type
  ```javascript
  import { createPipeBuilder } from 'pipe-builder-to-string';
  const __ = createPipeBuilder();
  const pipe = __;
  pipe.g.V().inE().outV();
  pipe.toString();
  // error
  ```

  Hopefully, there is a decent solution for this, but currently, createPipeBuilder does not return a Builder object
    , but returns an object in which each property is a pipeName and upon being called:
      inits a Builder, calls the property name's function, and returns the Builder


  current work around
  ```javascript
    let pipe = __;
    pipe = pipe.g.V().inE().outV();
  ```
  Pretty fucking poor; no bueno.

  So, perhaps we can create a Builder when createPipeBuilder is called, then said Builder is passed into the first
    function call.

  or shit, what if this is one of those situations where .this is actually fucking desirable.
    if the call comes from __., then pass the builder into the first function call, if not don't pass it

  ...later

  yep, this doesn't work
    renamed createPipeBuilder to createPipeBuilderLib, to indicate that createPipeBuilderLib returns a lib of pipeBuilders
      rather than a pipeBuilder
